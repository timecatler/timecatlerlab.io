+++
menu = ""
description = ""
categories = ["windows", "programming", "web"]
tags = ["git", "github", "github pages", "hugo"]
date = "2017-02-07T19:46:15+03:00"
title = "Схема реализации сайта"
images = []
banner = "/banners/hugo.png"

+++

Давным давно, в далёкой-далёкой галактике у меня уже был недобложек.
Представлял он из себя wordpress, поднятый на бесплатном хостинге, тормозил и в целом перестал меня удовлетворять, как только из недобложика, служащего определённой цели я попытался сделать просто личный недобложик.
В итоге назрела необходимость его переделать, принципиальным в которой был только один пункт: держать новый я хотел на [Github Pages](https://pages.github.com/). Впрочем на пути стояло одно препятствие. Я не люблю web. Не люблю, не понимаю и никогда полноценно не занимался. Однако, хэй! Есть же генераторы статических веб-сайтов. Первый раз я ткнулся в [Madoko](https://www.madoko.net/). Миленько, гибрид $\LaTeX$ и Markdown, но поднять его у меня не получилось. К счастью, вторая попытка сработала в разы лучше. Познакомьтесь с [Hugo](https://gohugo.io/).
{{<figure src="/img/site/gopher.png">}}

<!--more-->

**Hugo** - это статический генератор сайтов, написанный, как легко можно понять, на `Golang`. Из плюсов: с ним просто работать, если умеешь во что-то в вебе. Из больших плюсов: с ним просто работать, если не особо умеешь во что-то в вебе.
### Теперь о процессе
Я поднимал всё это добро под Windows по весьма простой причине: Linux на текущей рабочей машине у меня на данный момент отсутствует. Для этого мне понадобились:

1. Сам [Hugo](https://github.com/spf13/hugo/releases);
2. Неплохая [тема](https://github.com/digitalcraftsman/hugo-icarus-theme) с поддержкой MathJax (частично $\LaTeX$) и весьма приличным видом;
3. Скрипты для деплоя на личную страницу от [этого чувака](https://github.com/Hjdskes/hjdskes.github.io/tree/hugo). Их я, впрочем, слегка подредачил;
4. Очевидно, аккаунт на GitHub;
5. [Git](https://git-scm.com/download/win). Вроде этот.

Саму схему реализации можно подсматривать [вот тут](https://github.com/timecatler/timecatler.github.io).
Разберёмся, что есть что:

1. Hugo генерирует сайтик по содержимому репозитория в папку `public`;
2. Вот [здесь](https://github.com/timecatler/timecatler.github.io/tree/hugo/themes) лежит тема;
3. В [конфиге](https://github.com/timecatler/timecatler.github.io/blob/hugo/config.toml) прописана она и ещё кое-какая информация;
4. Вот [так](https://raw.githubusercontent.com/timecatler/timecatler.github.io/hugo/content/post/kernel_modules.md) выглядит первый пост;
5. Вот [здесь](https://github.com/timecatler/timecatler.github.io/tree/hugo/static) лежит всё то добро, которое потом попадает в корень сайта;
6. Вот [этот скрипт](https://github.com/timecatler/timecatler.github.io/blob/hugo/setup.sh) инициализирует репозиторий.
7. Вот [этот](https://github.com/timecatler/timecatler.github.io/blob/hugo/deploy.sh) обновляет версию сайта.

Теперь, когда всё это разложено по полочкам, можно разобраться со скриптами от славного [Hjdskes](https://github.com/Hjdskes).

### Первый
{{< highlight bash >}}
#!/usr/bin/env bash

# This script does the required work to set up your personal GitHub Pages
# repository for deployment using Hugo. Run this script only once -- when the
# setup has been done, run the `deploy.sh` script to deploy changes and update
# your website. See
# https://hjdskes.github.io/blog/deploying-hugo-on-personal-github-pages/index.html
# for more information.

# GitHub username
USERNAME=timecatler
# Name of the branch containing the Hugo source files.
SOURCE=hugo

msg() {
    printf "\033[1;32m :: %s\n\033[0m" "$1"
}

msg "Deleting the \`master\` branch"
git branch -D master
git push origin --delete master

msg "Creating an empty, orphaned \`master\` branch"
git checkout --orphan master
git rm --cached $(git ls-files)

msg "Grabbing one file from the \`$SOURCE\` branch so that a commit can be made"
git checkout "$SOURCE" README.md
git commit -m "Initial commit on master branch"
git push origin master

msg "Returning to the \`$SOURCE\` branch"
git checkout -f "$SOURCE"

msg "Removing the \`public\` folder to make room for the \`master\` subtree"
rm -rf public
git add -u
git commit -m "Remove stale public folder"

msg "Adding the new \`master\` branch as a subtree"
git subtree add --prefix=public \
    https://github.com/$USERNAME/$USERNAME.github.io.git master --squash

msg "Pulling down the just committed file to help avoid merge conflicts"
git subtree pull --prefix=public \
https://github.com/$USERNAME/$USERNAME.github.io.git master
{{< /highlight >}}
По порядку

1. Удаляет ветку `master`;
2. Создаёт пустую ветку `master`;
3. Создаёт стартовый коммит из `README.md` и пушит его в репозиторий;
4. Возвращается на исходную ветку (в нашем случае `hugo`);
5. Удаляет папку `public`;
6. Делает хитрый финт ушами, в результате которого папкой `public` становится ветка `master`;
7. Вытягивает из репозитория стартовый коммит ветки `master`.

Для чего это нужно? Личные сайты хранятся в корне ветки `master`.
Весь этот грязный хак позволяет хранить сырцы в одной ветке (например `hugo`), а готовый сайт в ветке `master`.
### Второй
{{< highlight bash >}}
#!/usr/bin/env bash

# This script allows you to easily and quickly generate and deploy your website
# using Hugo to your personal GitHub Pages repository. This script requires a
# certain configuration, run the `setup.sh` script to configure this. See
# https://hjdskes.github.io/blog/deploying-hugo-on-personal-github-pages/index.html
# for more information.

# Set the English locale for the `date` command.
export LC_TIME=en_US.UTF-8

# GitHub username.
USERNAME=timecatler
# Name of the branch containing the Hugo source files.
SOURCE=hugo
# The commit message.
MESSAGE="Site rebuild $(date)"

msg() {
    printf "\033[1;32m :: %s\n\033[0m" "$1"
}

msg "Pulling down the \`master\` branch into \`public\` to help avoid merge conflicts"
git subtree pull --prefix=public \
    https://github.com/$USERNAME/$USERNAME.github.io.git origin master -m "Merge origin master"

msg "Building the website"
hugo

msg "Pushing the updated \`public\` folder to the \`$SOURCE\` branch"
git add public
git commit -m "$MESSAGE"
git push origin "$SOURCE"

msg "Pushing the updated \`public\` folder to the \`master\` branch"
git subtree push --prefix=public \
https://github.com/$USERNAME/$USERNAME.github.io.git master
{{< /highlight >}}
По порядку:

1. Вытягиваем ветку `master` в папку `public`, чтобы избежать конфликтов;
2. Пересобираем сайт;
3. Пушим обновлённый сайт в исходную ветку (в нашем случае `hugo`);
4. Пушим обновлённый сайт в ветку `master`.

Всё. Вот такая вот прелесть.

### upd 28.05.2018

Сайт переехал на `Gitlab Pages` и большая часть текста выше попросту не нужна.
Дело в том, что у Gitlab в отличие от Github размещение статичных сайтов происходит несколько умнее.
Они используют приёмы непрерывной интеграции и собирают через докер сайт, а затем хостят результат сборки.
Так что место этой кучи костылей занял [этот](https://gitlab.com/timecatler/timecatler.gitlab.io/blob/master/.gitlab-ci.yml) файл конфигурации.
В нём, кстати, тоже есть костыль, чтобы тема нормально работала, но он не настолько грязный.
